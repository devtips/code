numbers = [1, 2, 3, 4]

doubled_numbers = []

for number in numbers:
    doubled_number = number * 2
    doubled_numbers.append(doubled_number)

print(doubled_numbers)
